[![Build Status](https://travis-ci.org/testobject/appium-test-setup-intermediate.svg)](https://travis-ci.org/testobject/appium-test-setup-intermediate)

# appium-test-setup-intermediate
A setup that allows you to run tests on the TestObject platform, exemplified through two basic tests on the [Calculator app](https://github.com/aluedeke/calculator) by Andreas Lüdeke. It differs from the [basic setup](https://github.com/testobject/appium-test-setup-basic/) in that it registers the results of said tests so the user can see them through the online UI.

# setting up jenkins ci  
1. uncomment env variable fetching in test class
2. add your testobject_api_key as encrypted enviroment variables in jenkins
3. add testobject device as environment variables in jenkins

# running from shell
1. uncomment api key and device capabilities in test class
2. set gradlew permissions: sudo chmod +x ./graldew
3. run tests: ./gradlew clean test -i
